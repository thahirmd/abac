/*
 * Create Headers Plugin
 *
 * @param id       - the identifier returned by the collector
 * @param content  - the output of your content plugin
 * @param options  - an object containing options. Options are sent from Java
 *
 * @return - an object of headers
 */
function createHeaders(id, content, options) {
  return {
  'lineage': {
      'harmonization': {
         'date': fn.currentDateTime(),
         'user': xdmp.getCurrentUser(),
         'description': 'Initial Version',
         'entity': options.entity,
         'flowName': options.flow,
         'flowType': options.flowType,
         'codeVersion': "1.0.1"
      }
    }
  };
}

module.exports = {
  createHeaders: createHeaders
};
