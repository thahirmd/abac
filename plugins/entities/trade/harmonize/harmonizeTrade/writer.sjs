/*~
 * Writer Plugin
 *
 * @param id       - the identifier returned by the collector
 * @param envelope - the final envelope
 * @param options  - an object options. Options are sent from Java
 *
 * @return - nothing
 */
function write(id, envelope, options) {
  let lUri = fn.replace(id, '/stage', '');
  let lCollections = [options.entity, 'Final', 'BAML','CS'];
  /*
  let lPermissions = [xdmp.permission("lob-all", "read"),
                      xdmp.permission("region-all", "read"),
                      xdmp.permission("lob-admin", "read"),
                      xdmp.permission("lob-admin", "update"),
                      xdmp.permission("region-admin", "read"),
                      xdmp.permission("region-admin", "update"),
                      xdmp.permission("pii-reader", "read"),
                      xdmp.permission("data-hub-role", "read"),
                      xdmp.permission("data-hub-role", "update"),
                      xdmp.permission("hub-admin-role", "read"),
                      xdmp.permission("hub-admin-role", "update"),
                      xdmp.permission("baml-role", "read"),
                      xdmp.permission("baml-role", "update")
                     ];
  let lob     = envelope.envelope.instance.trade.lob;
  let region  = envelope.envelope.instance.trade.region;

  lob.toString().toUpperCase().trim();
  region.toString().toUpperCase().trim();
  xdmp.log('id: '+id+'  lob: '+lob+ ' region:'+region);

  if (lob == 'RATES') {
    lPermissions.push(xdmp.permission("lob-rates", "read"));
    xdmp.log( lob+' matched');}
  else {
    xdmp.log(lob+' didnt match');
  }

  if (region == 'AMER') {
    lPermissions.push(xdmp.permission("region-amer", "read"));}
  else if(region == "EMEA") {
    lPermissions.push(xdmp.permission("region-emea", "read"));
  }
  */
  let lPermissions = [
                      xdmp.permission("pii-reader", "read"),
                      xdmp.permission("data-hub-role", "read"),
                      xdmp.permission("data-hub-role", "update"),
                      xdmp.permission("hub-admin-role", "read"),
                      xdmp.permission("hub-admin-role", "update"),
                      xdmp.permission("baml-role", "read"),
                      xdmp.permission("baml-role", "update")
                     ];

  xdmp.documentInsert(lUri, envelope, lPermissions, lCollections );
}

module.exports = write;
