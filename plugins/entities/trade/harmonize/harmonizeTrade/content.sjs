'use strict'

/*
* Create Content Plugin
*
* @param id         - the identifier returned by the collector
* @param options    - an object containing options. Options are sent from Java
*
* @return - your content
*/
function createContent(id, options) {
  let doc = cts.doc(id);

  let source;

  // for xml we need to use xpath
  if(doc && xdmp.nodeKind(doc) === 'element' && doc instanceof XMLDocument) {
    source = doc
  }
  // for json we need to return the instance
  else if(doc && doc instanceof Document) {
    source = fn.head(doc.root);
  }
  // for everything else
  else {
    source = doc;
  }

  return extractInstanceTrade(source);
}
  
/**
* Creates an object instance from some source document.
* @param source  A document or node that contains
*   data for populating a trade
* @return An object with extracted data and
*   metadata about the instance.
*/
function extractInstanceTrade(source) {
  // the original source documents
  let attachments = source;
  // now check to see if we have XML or json, then create a node clone from the root of the instance
  if (source instanceof Element || source instanceof ObjectNode) {
    let instancePath = '/*:envelope/*:attachments';
    if(source instanceof Element) {
      //make sure we grab content root only
      instancePath += '/node()[not(. instance of processing-instruction() or . instance of comment())]';
    }
    source = new NodeBuilder().addNode(fn.head(source.xpath(instancePath))).toNode();
  }
  else{
    source = new NodeBuilder().addNode(fn.head(source)).toNode();
  }
  attachments = source;
  let docId = !fn.empty(fn.head(source.xpath('/docId'))) ? xs.string(fn.head(fn.head(source.xpath('/docId')))) : null;
  let lob = !fn.empty(fn.head(source.xpath('/lob'))) ? xs.string(fn.head(fn.head(source.xpath('/lob')))) : null;
  let region = !fn.empty(fn.head(source.xpath('/region'))) ? xs.string(fn.head(fn.head(source.xpath('/region')))) : null;
  let salesId = !fn.empty(fn.head(source.xpath('/salesId'))) ? xs.string(fn.head(fn.head(source.xpath('/salesId')))) : null;
  let qty = !fn.empty(fn.head(source.xpath('/qty'))) ? xs.decimal(fn.head(fn.head(source.xpath('/qty')))) : null;
  let description = !fn.empty(fn.head(source.xpath('/description'))) ? xs.string(fn.head(fn.head(source.xpath('/description')))) : null;

  // return the instance object
  return {
    '$attachments': attachments,
    '$type': 'trade',
    '$version': '0.0.1',
    'docId': docId,
    'lob': lob,
    'region': region,
    'salesId': salesId,
    'qty': qty,
    'description': description
  }
};


function makeReferenceObject(type, ref) {
  return {
    '$type': type,
    '$ref': ref
  };
}

module.exports = {
  createContent: createContent
};

