xquery version "1.0-ml";
import module namespace
  trgr = 'http://marklogic.com/xdmp/triggers'
  at '/MarkLogic/triggers.xqy';
declare variable $trgr:uri as xs:string external;
declare variable $trgr:trigger as node() external;

declare variable $currentPermissions := xdmp:document-get-permissions($trgr:uri);
declare variable $newPermissions := (xdmp:permission("lob-all", "read"),
                                     xdmp:permission("region-all", "read"),
                                     xdmp:permission("lob-admin", "read"),
                                     xdmp:permission("lob-admin", "update"),
                                     xdmp:permission("region-admin", "read"),
                                     xdmp:permission("region-admin", "update"));
declare variable $doc := fn:doc($trgr:uri);
declare variable $lob     := $doc/envelope/instance/trade/lob;
declare variable $region  := $doc/envelope/instance/trade/region;

let $newPermissions :=
      if (fn:upper-case($lob) = 'RATES') then
        fn:insert-before($newPermissions,1,xdmp:permission("lob-rates", "read"))
      else ($newPermissions)

let $newPermissions :=
      if ($region = 'AMER') then
         fn:insert-before($newPermissions,1,xdmp:permission("region-amer", "read"))
      else if ($region = "EMEA") then
         fn:insert-before($newPermissions,1,xdmp:permission("region-emea", "read"))
      else ($newPermissions)

let $completeSetPermissions := ($currentPermissions, $newPermissions)

return xdmp:document-set-permissions($trgr:uri, $completeSetPermissions)
