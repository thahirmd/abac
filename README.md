## MarkLogic Attribute Based Access Control (ABAC) and Compartment Security ##

MarkLogic support Role Based Access Control. Attribute Based Access Control is implemented using a trigger which checks for the attributes and add the necessary roles to the document.

This project also leverage MarkLogic's compartment security.

Please read the requirements.xlsx document to understand the requirements. All passwords can be found at the user definition at ./src/ml-config/security/roles

Other codes of interest:
   triggers:  src/main/ml-config/databases/data-hub-FINAL/triggers
	 trigger modules: /src/main/ml-modules
	 Roles,Privileges,users:  /src/main/ml-config/Security

The project uses two compartments. Pls read requirements.xlsx

### Prerequisites ###

1. MarkLogic Data Hub Framework
2. MarkLogic database


### Setup ###


1. run ./gradlew mlDeploy  which will deploy the project
2. run ./gradlew loadData  (adjust your path to input)
3. run ./gradlew harmonizeData

### Next Steps ###
Add element level security on top
